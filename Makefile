include lib.mk
.PHONY: meld-build meld-run meld-flatpak

BUILD_DIR ?= /build

#--userns=keep-id
meld-run:
	podman volume create --label owner=flatpak-meld flatpak-meld-build
	podman run --rm -it --read-only \
	  --privileged --device /dev/fuse --cap-add CAP_SYS_ADMIN --cap-add CAP_SYS_CHROOT --cap-add CAP_NET_ADMIN \
	  --env HOME=/build/home \
	  --volume flatpak-meld-build:/build:exec \
	  --volume .:/src:Z,ro \
	  --workdir /src \
	  registry.gitlab.com/ors-it-oss/oci:flatpak bash
	restorecon -RrF .
	#podman volume rm flatpak-meld-build
	#podman volume rm flatpak-meld-cache

#meld-build:
#	podman run --userns=keep-id --rm -it \
#	  --env HOME=/flatpak \
#	  --tmpfs /build:exec \
#	  --tmpfs /flatpak:exec \
#	  --volume .:/src \
#	  --workdir /src \
#	  quay.io/theloeki/build:flatpak make meld-flatpak

meld-flatpak:
	export HOME=$(BUILD_DIR)/home
	flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
	flatpak install --user --noninteractive -vvy flathub org.gnome.{Platform,Sdk}//3.34
#--state-dir=/build/flatpak-builder --disable-rofiles-fuse 
	flatpak-builder -v \
	  --force-clean \
	  --state-dir=$(BUILD_DIR)/state --repo=$(BUILD_DIR)/repo \
	  $(BUILD_DIR)/flatpak meld/com.asml.meld.json
	flatpak build-bundle $(BUILD_DIR)/repo $(BUILD_DIR)/mymeld.flatpak com.asml.meld
